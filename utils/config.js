'use strict';

const fs = require('fs');
const nconf = require('nconf');

// priority : arguments, followed by env variables, followed by the config file
nconf.argv().env().file({ file: 'config.json' });

var config = {
    rhost : nconf.get('rethinkdb:host'),
    rport : nconf.get('rethinkdb:port'),
    rdb : nconf.get('rethinkdb:db'),

    appPort : nconf.get('app:port'),
}

module.exports = config;