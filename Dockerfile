FROM julienlengrand/alpine-node-rethinkdb

# Define mountable directories.
VOLUME ["/data"]

# Define working directory.
WORKDIR /data

# Install app dependencies
COPY package.json /data
RUN npm install

# # Bundle app source
COPY . /data

# # Expose rethinkdb ports.
#   - 8080: web UI
#   - 28015: process
#   - 29015: cluster
EXPOSE 8080
#EXPOSE 28015
#EXPOSE 29015

# Expose node app ports
EXPOSE 4567

CMD [ "/bin/sh", "/data/startApp.sh" ]
