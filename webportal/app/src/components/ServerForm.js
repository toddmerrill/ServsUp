import React, { Component, PropTypes } from 'react';
import SaveIcon from 'react-icons/lib/md/save';

class ServerForm extends Component {

  constructor(props){
    super(props);
    this.props = props;
    this.state = {
      inputValues: {
        serverName: props.server.serverName || '',
        serverUrl: props.server.url || props.server.serverUrl || '',
      }
    };

    this.onInputChange = this.onInputChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  componentDidMount() {
  }

  onSubmit(e) {
    e.preventDefault();
    const { inputValues } = this.state;
    const { server } = this.props;

    // TODO: Add validation

    const newServer = Object.assign({}, server, inputValues);
    this.props.onSubmit(e, newServer);
  }

  onInputChange(key, e) {
    const value = e.currentTarget.value;
    this.state.inputValues[key] = value;
    this.forceUpdate();
  }

  render() {
    return (
        <div className="server-form">
          <h3>Server Form</h3>
          <form onSubmit={this.onSubmit}>
            <input
              type="text"
              placeholder="Server Name"
              value={this.state.inputValues.serverName}
              onChange={this.onInputChange.bind(this, 'serverName')}
            />
            <input
              type="text"
              placeholder="Server URL"
              value={this.state.inputValues.serverUrl}
              onChange={this.onInputChange.bind(this, 'serverUrl')}
            />
            <button type="submit" className="save">
              Save
            </button>
          </form>
        </div>
    );
  }
}

ServerForm.propTypes = {
};

export default ServerForm;
