
export const createConstants = (function createConstants() {
  let arr = {};
  return function constantsReducer(...args) {
    arr = args.reduce((acc, constant) => {
      acc[constant] = constant;
      return acc;
    }, arr);

    return arr;
  };
}());

export default createConstants;
