'use strict';

const Hapi = require('hapi');
const r = require('rethinkdb');
const config = require('./utils/config.js');
const server = new Hapi.Server();
const routes = require('./routes');

// Starts the worker as part of the main thread.
require('./workers/worker.js');

server.connection({
  port: config.appPort,
  routes: {
    cors: true,
  },
});
server.register([
  require('inert'),
  require('hapi-io')], (err) => {
  if (err) throw err;
});

server.route(routes);

server.start((err) => {
  if (err) throw err;
  console.log('Server running at: ' + server.info.uri);
});

r.connect({
    host: config.rhost,
    port: config.rport,
    db: config.rdb
  }, (err, conn) => {
  if (err) throw err;
  getChangeFeed(conn);
});

function getChangeFeed(connection) {
  r.table('servers')
    .changes()
    .run(connection, (err, cursor) => {
      const io = server.plugins['hapi-io'].io;
      cursor.each((err, change) => {
        if (err) throw err;
        io.emit('status_change', change);
      });
    });
}
